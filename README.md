# README #
# This is Homework 2 for MTH9815 from Huiyou Chen and Weihao Li #
# This project contains 4 python files and 2 data files. 

1. hw2.py implements the main function. 
you will be able to use command: python hw3.py -y yc.csv -d mortgages.csv
to load two .csv files and print out the results

2. Yield_Curve.py implements the Yield_Curve class, which has two constructors, one is to get the zero rates curve, the other one is used for linear interplotation

3. Mortgage.py implements the Mortgage class, which contains member functions to get zero rates and pricing the mortgage itself, compute duration/convexity/DV01

4. Portfolio.py implements the Portfolio class, which specifically contains a member function addMortgage() to add mortgages, and also compute its price/duration/convexity/DV01.