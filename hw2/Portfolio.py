import math

class Portfolio(object):
    # Packaged mortgate securities

    def __init__(self, zero_curve):
        # Initialize the portfolio
        self._zero_curve = zero_curve
        # a list of all mortgages
        self._mortgages = []

    def modify_curve(self, zero_curve):
        # get a new zero_curve
        self._zero_curve = zero_curve

    def addMortgage(self, mortgage):
        # insert a mortgage into this package
        self._mortgages.append(mortgage)
        print "Insert Mortgage --> {}".format(mortgage)


    def Pricing(self, offset = 0):
        # Pricing this package
        price = 0
        for mortgage in self._mortgages:
            price += mortgage.Pricing(self._zero_curve, offset)
        return price

    def Duration(self, shock):
        # Compute modified duration of this portfolio
        return math.log(self.Pricing() / self.Pricing(shock)) / (shock / 10000.)

    def Convexity(self, shock):
        # Compute convexity of this portfolio
        price_plus = self.Pricing(shock)
        price_minus = self.Pricing(-shock)
        price = self.Pricing()
        return (price_plus - 2 * price + price_minus) / ((shock / 10000.)** 2) / price

    def DV01(self, shock):
        # Compute DV01 of this portfolio
        return -(self.Pricing(shock / 20.) - self.Pricing(-shock / 20.)) / (shock / 10.)
