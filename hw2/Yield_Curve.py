# Yield_Curve class

import scipy.interpolate

class Yield_Curve(object):

    def __init__(self, yc):
        # Initialize the YieldCurve,
        # yc: a disctionary with keys: terms, values: yields
        self._yc = yc
        self._rate_interp = scipy.interpolate.interp1d(yc.keys(), yc.values())

    def __call__(self, t):
        # return yield curve using linear interpolation
        return self._rate_interp(t)
