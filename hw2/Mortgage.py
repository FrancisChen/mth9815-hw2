# Mortgage class

import math

class Mortgage(object):

    def __init__(self, principal, rate, term):

        # Initialize a Mortgage
        self._principal = principal
        self._rate = rate
        self._term = term
        # term in months
        self._monthlyterm = term * 12
        # monthly rate
        self._monthlyrate = rate / 12.
        # monthly payment
        self._payment = self._principal * (self._monthlyrate / 100. * (1. + self._monthlyrate / 100.) ** self._monthlyterm) / ((1. + self._monthlyrate / 100.) ** self._monthlyterm - 1)

    def __str__(self):
        # print the information of this mortgage
        return "Mortgage with Principal : {0}, Rate : {1}, Term :{2}".format(self._principal, self._rate, self._term)

    def Pricing(self, zero_curve, offset=0):
        # Pricing the mortgage from zero curve
        # df : a list of discount factor
        df = []
        for t in range(1, self._monthlyterm + 1):
            df.append(1. / (1. + zero_curve(t) / 2. / 100. + offset / 10000. / 2.)**(t / 6.))

        value = self._payment * sum(df)
        return value

    def Duration(self, zero_curve, shock):
        """
        Compute the modified duration.
        ModD = - (log(V+h) - log(V)) / h
        """
        return math.log(self.Pricing(zero_curve) / self.Pricing(zero_curve, shock)) / (shock / 10000.)

    def Convexity(self, zero_curve, shock):
        # Compute the convexity using finite difference
        price_plus = self.Pricing(zero_curve, shock)
        price_minus = self.Pricing(zero_curve, -shock)
        price = self.Pricing(zero_curve)
        return (price_plus - 2 * price + price_minus) / ((shock / 10000.) ** 2) / price

    def DV01(self, zero_curve, shock):
        # Compute DV01 using finite difference
        return (self.Pricing(zero_curve) * self.Duration(self, zero_curve, shock)) / 10000.
