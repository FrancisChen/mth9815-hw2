import argparse
import csv
from Yield_Curve import Yield_Curve
from Mortgage import Mortgage
from Portfolio import Portfolio

if __name__ == "__main__":
    myParser = argparse.ArgumentParser()
    myParser.add_argument("-y", help="load yield curve", required=True)
    myParser.add_argument("-d", help="load portfolio of mortgages", required=True)
    args = myParser.parse_args()

    # read csv file of rate
    my_YC = {}
    with open(args.y, 'r') as YC_file:
        YC_reader = csv.reader(YC_file)
        next(YC_reader)
        for row in YC_reader:
            term, rate = map(float, row)
            my_YC[term] = rate

    # use pkg to store mortgages
    my_curve = Yield_Curve(my_YC)
    my_portfolio = Portfolio(my_curve)
    with open(args.d, 'r') as Mortgage_file:
        portfolio_reader = csv.reader(Mortgage_file)
        next(portfolio_reader)
        for row in portfolio_reader:
            principal, r, t = map(float, row)
            my_portfolio.addMortgage(Mortgage(principal, r, int(t)))
    shock = 10.
    print "---------------------------------------------"
    print "--                Part 01                  --"
    print "---------------------------------------------"
    print "The PRICE of this portfolio is ${}".format(my_portfolio.Pricing())
    print " "

    print "---------------------------------------------"
    print "--                Part 02                  --"
    print "---------------------------------------------"
    print "The DURATION of this portfolio is {}".format(my_portfolio.Duration(shock))
    print "The DV01 of this portfolio is {}".format(my_portfolio.DV01(shock))
    print "The CONVEXITY of this portfolio is {}".format(my_portfolio.Convexity(shock))
    print " "